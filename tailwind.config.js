// const { colors } = require('tailwindcss/defaultTheme')

module.exports = {
  theme: {
    container: {
      center: true,
      padding: '2rem'
    },    
    fontFamily: {
      sans: [
        '"Source Sans Pro"',
        '-apple-system',
        'BlinkMacSystemFont',
        '"Segoe UI"',
        'Roboto',
        '"Helvetica Neue"',
        'Arial',
        '"Noto Sans"',
        'sans-serif',
        '"Apple Color Emoji"',
        '"Segoe UI Emoji"',
        '"Segoe UI Symbol"',
        '"Noto Color Emoji"'
      ],
    },
    spacing: {
      none: '0',
      sm: '0.25rem',
      md: '0.75rem',
      lg: '2.25rem',
      xl: '6.75rem'
    },
    fontSize: {
      xs: '0.64rem',
      sm: '0.8rem',
      base: '1rem',
      lg: '1.25rem',
      xl: '1.5625rem',
      xxl: '2.953125rem'
    },
    borderRadius: {
      none: '0',
      sm: '0.25rem',
      default: '1.25rem',
      lg: '2rem',
      full: '9999px',
    },
    extend: {
      colors: {
        primary: '#14cdab',
        dark: '#062635',
        accent: '#dd6b20'
      },
      borderWidth: {
        '6': '6px',
      },  
      opacity: {
        '90': '0.90',
      },  
      zIndex: {
        '-1': '-1'
      },
      inset: {
        '-half': '-50%',
        '100': '100%',
      },
    }
  },
  variants: {},
  plugins: []
}
